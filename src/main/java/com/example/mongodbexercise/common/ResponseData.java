package com.example.mongodbexercise.common;

public class ResponseData {
  protected Object data;
  protected String status;
  protected String message;
  protected String ex;

  public ResponseData() {
  }

  public ResponseData(String status, Object data) {
    this.status = status;
    this.data = data;
  }

  public ResponseData(String status, String message) {
    this.status = status;
    this.message = message;
  }

  public ResponseData(String status, String message, Object data) {
    this.data = data;
    this.status = status;
    this.message = message;
  }

  public ResponseData(Object data, String status, String message, String ex) {
    this.data = data;
    this.status = status;
    this.message = message;
    this.ex = ex;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getEx() {
    return ex;
  }

  public void setEx(String ex) {
    this.ex = ex;
  }
}
