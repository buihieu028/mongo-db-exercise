package com.example.mongodbexercise.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Pagination {
  @JsonProperty(value = "page")
  private long currentPage;

  @JsonProperty(value = "size")
  private long pageSize;

  @JsonProperty(value = "totalPage")
  private long totalPage;

  @JsonProperty(value = "totalRecords")
  private long totalRecords;

  public Pagination() {

  }

  public Pagination(long currentPage, long pageSize, long totalPage, long totalRecords) {
    this.currentPage = currentPage;
    this.pageSize = pageSize;
    this.totalPage = totalPage;
    this.totalRecords = totalRecords;
  }

  public long getCurrentPage() {
    return currentPage;
  }

  public void setCurrentPage(long currentPage) {
    this.currentPage = currentPage;
  }

  public long getPageSize() {
    return pageSize;
  }

  public void setPageSize(long pageSize) {
    this.pageSize = pageSize;
  }

  public long getTotalPage() {
    return totalPage;
  }

  public void setTotalPage(long totalPage) {
    this.totalPage = totalPage;
  }

  public long getTotalRecords() {
    return totalRecords;
  }

  public void setTotalRecords(long totalRecords) {
    this.totalRecords = totalRecords;
  }
}
