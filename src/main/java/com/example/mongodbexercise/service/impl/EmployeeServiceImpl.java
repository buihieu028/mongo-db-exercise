package com.example.mongodbexercise.service.impl;

import com.example.mongodbexercise.exception.BusinessException;
import com.example.mongodbexercise.model.Employee;
import com.example.mongodbexercise.repository.EmployeeRepository;
import com.example.mongodbexercise.service.EmployeeService;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class EmployeeServiceImpl implements EmployeeService {

  @Autowired
  EmployeeRepository employeeRepository;

  @Override
  public Page<Employee> listEmployeeFilter(String userId, String inviteCode, String email, Pageable pageable) {
    List<Employee> employeeList = employeeRepository.filterEmployee(userId, inviteCode, email, pageable);
    return new PageImpl<>(employeeList, pageable, employeeList.isEmpty() ? 0 : employeeList.size());

  }

  @Override
  public Employee findById(String id) {
    return employeeRepository.findById(id).orElse(null);
  }

  @Override
  public Employee createNewEmployee(String email) throws Exception {
    if (!checkEmail(email)) {
      throw new BusinessException("EMAIL_INVALID");
    }
    Employee employee = new Employee();
    employee.setEmail(email);

    Random rnd = new Random();
    int n = 100000 + rnd.nextInt(900000);
    employee.setInviteCode(String.valueOf(n));
    employee.setStatus(true);
    employee.setKycLevel(0);
    employee.setActiveUser(true);
    employee.setDeleted(false);
    employee.setWithdraw(false);
    employee.setLockWithdraw(false);
    employee.setCreated_at(new Date());
    employee.setUpdated_at(new Date());

    Employee saveEmployee = employeeRepository.save(employee);
    return saveEmployee;
  }

  @Override
  public void exportExcel(String user_id, String invite_code, String email, HttpServletResponse response)
    throws IOException {
    HSSFWorkbook workbook = new HSSFWorkbook();
    HSSFSheet sheet = workbook.createSheet("Employee_sheet");
    sheet.setDefaultRowHeight((short) 500);

    List<Employee> employeeList = employeeRepository.filterEmployee(user_id, invite_code, email, null);

    int rowNum = 0;
    Cell cell;
    Row row;

    HSSFCellStyle style = createStyleForTitle(workbook);
    row = sheet.createRow(rowNum);

    //id
    cell = row.createCell(0, CellType.STRING);
    cell.setCellValue("UserID");
    cell.setCellStyle(style);

    //invite_code
    cell = row.createCell(1, CellType.STRING);
    cell.setCellValue("Invite Code");
    cell.setCellStyle(style);

    //email
    cell = row.createCell(2, CellType.STRING);
    cell.setCellValue("email");
    cell.setCellStyle(style);

    //status
    cell = row.createCell(3, CellType.STRING);
    cell.setCellValue("status");
    cell.setCellStyle(style);

    //kyc_level
    cell = row.createCell(4, CellType.STRING);
    cell.setCellValue("Kyc Level");
    cell.setCellStyle(style);

    //active_user
    cell = row.createCell(5, CellType.STRING);
    cell.setCellValue("Active User");
    cell.setCellStyle(style);

    //deleted
    cell = row.createCell(6, CellType.STRING);
    cell.setCellValue("Deleted");
    cell.setCellStyle(style);

    //withdraw
    cell = row.createCell(7, CellType.STRING);
    cell.setCellValue("Withdraw");
    cell.setCellStyle(style);

    //Lock withdraw
    cell = row.createCell(8, CellType.STRING);
    cell.setCellValue("Lock withdraw");
    cell.setCellStyle(style);

    //created_at
    cell = row.createCell(9, CellType.STRING);
    cell.setCellValue("Created At");
    cell.setCellStyle(style);

    //updated_at
    cell = row.createCell(10, CellType.STRING);
    cell.setCellValue("Updated At");
    cell.setCellStyle(style);

    for (Employee employee : employeeList) {
      rowNum++;
      row = sheet.createRow(rowNum);

      cell = row.createCell(0, CellType.STRING);
      cell.setCellValue(employee.getId());

      cell = row.createCell(1, CellType.STRING);
      cell.setCellValue(employee.getInviteCode());

      cell = row.createCell(2, CellType.STRING);
      cell.setCellValue(employee.getEmail());

      cell = row.createCell(3, CellType.BOOLEAN);
      cell.setCellValue(employee.isStatus());

      cell = row.createCell(4, CellType.NUMERIC);
      cell.setCellValue(employee.getKycLevel());

      cell = row.createCell(5, CellType.BOOLEAN);
      cell.setCellValue(employee.isActiveUser());

      cell = row.createCell(6, CellType.BOOLEAN);
      cell.setCellValue(employee.isDeleted());

      cell = row.createCell(7, CellType.BOOLEAN);
      cell.setCellValue(employee.isWithdraw());

      cell = row.createCell(8, CellType.BOOLEAN);
      cell.setCellValue(employee.isLockWithdraw());

      cell = row.createCell(9, CellType.STRING);
      SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
      String date = formatter.format(employee.getCreated_at());
      cell.setCellValue(date);

      cell = row.createCell(10, CellType.STRING);
      String date2 = formatter.format(employee.getUpdated_at());
      cell.setCellValue(date2);
    }

    ServletOutputStream outputStream = response.getOutputStream();
    workbook.write(outputStream);
  }

  public HSSFCellStyle createStyleForTitle(HSSFWorkbook workbook) {
    HSSFFont font = workbook.createFont();
    font.setBold(true);
    HSSFCellStyle style = workbook.createCellStyle();
    style.setFont(font);
    style.setAlignment(HorizontalAlignment.CENTER);

    return style;
  }

  public boolean checkEmail(String email) {
    String regex = "^(.+)@(.+)$";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(email);
    return matcher.matches();

  }
}
