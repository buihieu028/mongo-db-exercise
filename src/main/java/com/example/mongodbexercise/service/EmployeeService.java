package com.example.mongodbexercise.service;

import com.example.mongodbexercise.model.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface EmployeeService {
  Page<Employee> listEmployeeFilter(String userId, String inviteCode, String email, Pageable pageable);

  Employee findById(String id);

  Employee createNewEmployee(String email) throws Exception;

  void exportExcel(String user_id, String invite_code, String email, HttpServletResponse response) throws IOException;
}