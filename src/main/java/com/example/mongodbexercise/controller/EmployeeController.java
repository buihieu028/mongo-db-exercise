package com.example.mongodbexercise.controller;

import com.example.mongodbexercise.common.Pagination;
import com.example.mongodbexercise.common.ResponseData;
import com.example.mongodbexercise.common.ResponseDataPagination;
import com.example.mongodbexercise.model.Employee;
import com.example.mongodbexercise.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/employee")
public class EmployeeController {

  @Autowired
  private EmployeeService employeeService;

  @GetMapping("/list-employee-by-filter")
  public ResponseData listAllEmployeeByFilter(@RequestParam(name = "user_id", required = false) String userId,
    @RequestParam(name = "invite_code", required = false) String inviteCode,
    @RequestParam(name = "email", required = false) String email,
    @RequestParam(name = "page", defaultValue = "1") int page,
    @RequestParam(name = "size", defaultValue = "10") int size) {
    ResponseDataPagination responseDataPagination = new ResponseDataPagination();
    Pagination pagination = new Pagination();

    int pageReq = page >= 1 ? page - 1 : page;
    Pageable pageable = PageRequest.of(pageReq, size);
    Page<Employee> employees = employeeService.listEmployeeFilter(userId, inviteCode, email, pageable);
    responseDataPagination.setData(employees.getContent());
    pagination.setCurrentPage(page);
    pagination.setPageSize(size);
    pagination.setTotalPage(employees.getTotalPages());
    pagination.setTotalRecords(employees.getTotalElements());
    responseDataPagination.setPagination(pagination);
    responseDataPagination.setStatus("SUCCESS");
    return responseDataPagination;
  }

  @GetMapping("/find-by-id")
  public ResponseData findById(@RequestParam(name = "id") String id) {
    Employee employee = employeeService.findById(id);
    if (employee != null) {
      return new ResponseData("SUCCESS", employee);
    }
    return new ResponseData("ERROR", "NOT FOUND");
  }

  @PostMapping("/create")
  public ResponseData createNewEmployee(@RequestBody Map<String, String> stringMap) {
    try {
      Employee employee = employeeService.createNewEmployee(stringMap.get("email"));
      return new ResponseData("SUCCESS", employee);
    } catch (Exception e) {
      return new ResponseData("ERROR", e.getMessage());
    }

  }

  @GetMapping("/export-excel")
  public void exportExcel(@RequestParam(name = "user_id", required = false) String userId,
    @RequestParam(name = "invite_code", required = false) String inviteCode,
    @RequestParam(name = "email", required = false) String email, HttpServletResponse response) throws IOException {

    response.setContentType("application/vnd.ms-excel");
    response.setHeader("Content-Disposition" , "attachment; filename=Employee.xls");
    employeeService.exportExcel(userId, inviteCode, email, response);

  }

}
