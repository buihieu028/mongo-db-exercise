package com.example.mongodbexercise.repository.impl;

import com.example.mongodbexercise.model.Employee;
import com.example.mongodbexercise.repository.EmployeeRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Repository
public class EmployeeRepositoryCustomImpl implements EmployeeRepositoryCustom {

  @Autowired
  MongoTemplate mongoTemplate;

  @Override
  public List<Employee> filterEmployee(String id, String inviteCode, String email, Pageable pageable) {
    Query query = new Query();

    if (pageable != null) {
      query.with(pageable);
    }

    List<Criteria> criteria = new ArrayList<>();
    //option i meaning case-sensitive , abc = ABC
    if (id != null) {
      criteria.add(Criteria.where("id").regex(id, "i"));

    }
    if (inviteCode != null) {
      criteria.add(Criteria.where("invite_code").regex(inviteCode, "i"));
    }
    if (email != null) {
      //query.addCriteria(Criteria.where("email").regex("/" + email + "/", "i"));
      criteria.add(Criteria.where("email").regex(email, "i"));

    }
    if (!criteria.isEmpty()) {
      query.addCriteria(new Criteria().andOperator(criteria.toArray(new Criteria[0])));
    }
    List<Employee> listEmployees = mongoTemplate.find(query, Employee.class);
    return listEmployees;

  }
}
