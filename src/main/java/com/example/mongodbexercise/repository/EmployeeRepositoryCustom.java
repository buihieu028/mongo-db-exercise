package com.example.mongodbexercise.repository;

import com.example.mongodbexercise.model.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface EmployeeRepositoryCustom {
  public List<Employee> filterEmployee(String id, String inviteCode, String email, Pageable pageable);
}
