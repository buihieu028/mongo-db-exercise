package com.example.mongodbexercise.repository;

import com.example.mongodbexercise.model.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EmployeeRepository extends MongoRepository<Employee, String>, EmployeeRepositoryCustom {

}
