package com.example.mongodbexercise.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

@Document(collection = "employee")
public class Employee {
  @Id
  private String id;

  @Field(name = "invite_code")
  private String inviteCode;

  @Field(name = "email")
  private String email;

  @Field(name = "status")
  private boolean status;

  @Field(name = "kyc_level")
  private int kycLevel;

  @Field(name = "active_user")
  private boolean activeUser;

  @Field(name = "deleted")
  private boolean deleted;

  @Field(name = "withdraw")
  private boolean withdraw;

  @Field(name = "lock_withdraw")
  private boolean lockWithdraw;
  @Field(name = "created_at")
  private Date created_at;

  @Field(name = "updated_at")
  private Date updated_at;

  public Employee() {
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getInviteCode() {
    return inviteCode;
  }

  public void setInviteCode(String inviteCode) {
    this.inviteCode = inviteCode;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public boolean isStatus() {
    return status;
  }

  public void setStatus(boolean status) {
    this.status = status;
  }

  public int getKycLevel() {
    return kycLevel;
  }

  public void setKycLevel(int kycLevel) {
    this.kycLevel = kycLevel;
  }

  public boolean isActiveUser() {
    return activeUser;
  }

  public void setActiveUser(boolean activeUser) {
    this.activeUser = activeUser;
  }

  public boolean isDeleted() {
    return deleted;
  }

  public void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }

  public boolean isWithdraw() {
    return withdraw;
  }

  public void setWithdraw(boolean withdraw) {
    this.withdraw = withdraw;
  }

  public boolean isLockWithdraw() {
    return lockWithdraw;
  }

  public void setLockWithdraw(boolean lockWithdraw) {
    this.lockWithdraw = lockWithdraw;
  }

  public Date getCreated_at() {
    return created_at;
  }

  public void setCreated_at(Date created_at) {
    this.created_at = created_at;
  }

  public Date getUpdated_at() {
    return updated_at;
  }

  public void setUpdated_at(Date updated_at) {
    this.updated_at = updated_at;
  }
}
